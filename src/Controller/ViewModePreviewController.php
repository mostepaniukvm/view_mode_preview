<?php

namespace Drupal\view_mode_preview\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\view_mode_preview\PreviewBuilder;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ViewModePreviewController.
 */
class ViewModePreviewController extends ControllerBase {

  /**
   * Preview helper service instance.
   *
   * @var \Drupal\view_mode_preview\PreviewBuilder
   */
  protected $previewBuilder;

  /**
   * Constructs a new ViewModePreviewController object.
   *
   * @param \Drupal\view_mode_preview\PreviewBuilder $preview_builder
   *   Preview helper service.
   */
  public function __construct(PreviewBuilder $preview_builder) {
    $this->previewBuilder = $preview_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('view_mode_preview.preview_builder')
    );
  }

  /**
   * Displays preview for a given entity type.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return array
   *   Renderable array.
   */
  public function preview($entity_type) {
    try {
      $entity_type_definition = $this->entityTypeManager()
        ->getDefinition($entity_type);
    }
    catch (Exception $e) {
      $this->messenger()->addError($e->getMessage());
      throw new NotFoundHttpException();
    }
    $build = [
      'filter_form' => $this->getFilterForm($entity_type_definition),
      'preview' => $this->previewBuilder->build($entity_type),
    ];

    return $build;
  }

  /**
   * Helper method to get filter form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition
   *   The entity type definition.
   *
   * @return array
   *   Renderable array of filter form.
   */
  protected function getFilterForm(EntityTypeInterface $entity_type_definition) {
    return $this->formBuilder()
      ->getForm('Drupal\view_mode_preview\Form\ViewModeFilterForm', $entity_type_definition);
  }

  /**
   * Page title callback.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return string
   *   Page title.
   */
  public function previewPageTitle($entity_type) {
    try {
      $entity_type_definition = $this->entityTypeManager()
        ->getDefinition($entity_type);
    }
    catch (Exception $e) {
      $this->messenger()
        ->addError($this->t('Unable to find @entity_type entity type: @message', [
          '@entity_type' => $entity_type,
          '@message' => $e->getMessage(),
        ]));
      throw new NotFoundHttpException();
    }

    return $entity_type_definition->getLabel();
  }

}
