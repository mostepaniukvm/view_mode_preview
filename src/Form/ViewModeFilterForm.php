<?php

namespace Drupal\view_mode_preview\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\view_mode_preview\PreviewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter form.
 */
class ViewModeFilterForm extends FormBase {

  /**
   * The EntityDisplayRepository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Preview helper service instance.
   *
   * @var \Drupal\view_mode_preview\PreviewBuilder
   */
  protected $previewBuilder;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Creates a ViewModeFilterForm instance.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity manager.
   * @param \Drupal\view_mode_preview\PreviewBuilder $preview_builder
   *   Preview helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_display_repository, PreviewBuilder $preview_builder, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityDisplayRepository = $entity_display_repository;
    $this->previewBuilder = $preview_builder;
    $this->entityTypeManager = $entity_type_manager;
    // @todo: unused.
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository'),
      $container->get('view_mode_preview.preview_builder'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_mode_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContentEntityTypeInterface $entity_type_definition = NULL) {
    // Entity type id from route parameter.
    $entity_type_name = $entity_type_definition->id();
    // Prepare view mode option list for given entity type.
    $view_modes = $this->entityDisplayRepository->getViewModes($entity_type_name);
    $view_mode_options = ['default' => 'Default'];
    foreach ($view_modes as $view_mode_name => $view_mode_data) {
      if (!empty($this->previewBuilder->getAvailableBundles($entity_type_name, $view_mode_name))) {
        $view_mode_options[$view_mode_name] = $view_mode_data['label'];
      }
    }

    $default_view_mode = $form_state->getValue('view_mode') ?? $this->getRequest()->query->get('view_mode') ?? 'default';
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#required' => TRUE,
      '#default_value' => $default_view_mode,
      '#options' => $view_mode_options,
      '#ajax' => [
        'callback' => [$this, 'bundleFieldsAjax'],
        'wrapper' => 'view-mode-filter-form',
      ],
    ];

    // Autocomplete fields.
    $form['select_entities'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['select_entities']['fields'] = $this->getEntityAutocompleteFields($entity_type_name, $default_view_mode);

    // Save value of entity type name to be able to use in form submit.
    $form_state->set('entity_type', $entity_type_name);

    // Add form actions.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview'),
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => [[$this, 'submitReset']],
    ];
    $form['actions']['preview-last'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview last entities'),
      '#submit' => [[$this, 'submitPreviewLast']],
    ];

    return $form;
  }

  /**
   * Prepare autocomplete form field.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $view_mode
   *   View mode name.
   *
   * @return array
   *  Renderable arrays of autocomplete fields.
   */
  protected function getEntityAutocompleteFields($entity_type, $view_mode) {
    $bundles = $this->previewBuilder->getAvailableBundles($entity_type, $view_mode);
    $fields = [];

    // Build autocomplete field for needed bundles.
    foreach ($bundles as $bundle) {

      $default_nid = $this->getRequest()->query->get($bundle);
      if ($default_nid) {
        $default = $this->entityTypeManager
          ->getStorage($entity_type)
          ->load($default_nid);
      }
      else {
        $default = '';
      }

      $fields[$bundle] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => $entity_type,
        '#default_value' => $default,
        '#title' => $this->t('Bundle: @bundle', ['@bundle' => $bundle]),
        '#attributes' => ['id' => $view_mode . $bundle],
      ];

      $entity_type_definition = $this->entityTypeManager
        ->getDefinition($entity_type);
      if ($entity_type_definition->hasKey('bundle')) {
        $fields[$bundle]['#selection_settings'] = ['target_bundles' => [$bundle => $bundle]];
      }
    }
    return $fields;
  }

  /**
   * Ajax handler to refresh autocomplete fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The form structure.
   */
  public function bundleFieldsAjax(array &$form, FormStateInterface $form_state) {
    $entity_type = $form_state->getValue('entity_type');
    $view_mode = $form_state->getValue('view_mode');

    $parameters = $this->previewBuilder->getValidatedParameters($entity_type, $view_mode);

    $response = new AjaxResponse();
    $url = Url::fromRoute('view_mode_preview.entity_preview', [
        'entity_type' => $entity_type,
        'view_mode' => $view_mode,
      ] + $parameters);
    $response->addCommand(new RedirectCommand($url->toString()));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update query parameters.
    $values = $form_state->getValues();
    $view_mode = $values['view_mode'];
    $bundles = $this->previewBuilder->getAvailableBundles($form_state->get('entity_type'), $view_mode);
    $parameters = [
      'view_mode' => $view_mode,
    ];

    // Prepare nids for available bundles.
    foreach ($bundles as $bundle) {
      if (isset($values['select_entities']['fields'][$bundle])) {
        $parameters[$bundle] = $values['select_entities']['fields'][$bundle];
      }
    }

    $this->getRequest()->query->replace($parameters);
  }

  /**
   * Form reset submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitReset(array &$form, FormStateInterface $form_state) {
    // Reset query parameters.
    $this->getRequest()->query->replace();
  }

  /**
   * Form submission handler to preview last entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitPreviewLast(array &$form, FormStateInterface $form_state) {
    // Update query parameters.
    $values = $form_state->getValues();
    $entity_type = $form_state->get('entity_type');

    $view_mode = $values['view_mode'];
    $bundles = $this->previewBuilder->getAvailableBundles($entity_type, $view_mode);
    $parameters = [
      'view_mode' => $view_mode,
    ];

    // Prepare nids for available bundles.
    foreach ($bundles as $bundle) {
      $parameters[$bundle] = $this->previewBuilder->getLastEntity($entity_type, $bundle);
    }

    $this->getRequest()->query->replace($parameters);
  }

}
